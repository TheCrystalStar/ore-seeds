package com.shadebyte.oreseeds;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.shadebyte.oreseeds.Core.SeedType;

public class OreSeedCMD implements CommandExecutor {

	private boolean isInt(String number) {
		try {
			Integer.parseInt(number);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {

		if (args.length == 0) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/oreseeds gui"));
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/oreseeds list"));
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/oreseeds give <player> <seed> <#>"));
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/oreseeds giveall <seed> <#>"));
			return true;
		}

		if (args.length == 1) {
			switch (args[0].toLowerCase()) {
			case "gui":
				if (sender instanceof Player) {
					Player p = (Player) sender;
					p.openInventory(Core.getInstance().seedInventory());
				} else {
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cOnly players can use the gui command."));
				}
				break;
			case "list":
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eEmerald, Lapis, Gold, Diamond, Redstone, Iron, Coal"));
				break;
			case "give":
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/oreseeds give &c<player> <seed> <#>"));
				break;
			case "giveall":
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/oreseeds &cgiveall <seed> <#>"));
				break;
			}
			return true;
		}

		if (args.length == 2) {
			switch (args[0].toLowerCase()) {
			case "give":
				Player target = Bukkit.getPlayer(args[1]);
				if (target != null) {
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/oreseeds give " + args[1] + " &c<seed> <#>"));
				} else {
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cThat player is currently offline."));
				}
				break;
			case "giveall":
				if (args[1].equalsIgnoreCase("emerald") || args[1].equalsIgnoreCase("lapis") || args[1].equalsIgnoreCase("gold") || args[1].equalsIgnoreCase("diamond")
						|| args[1].equalsIgnoreCase("redstone") || args[1].equalsIgnoreCase("iron") || args[1].equalsIgnoreCase("coal")) {
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/oreseeds &cgiveall " + args[1] + " &c<#>"));
				} else {
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cPlease enter a valid ore seed."));
				}
				break;
			}
			return true;
		}

		if (args.length == 3) {
			switch (args[0].toLowerCase()) {
			case "give":
				Player target = Bukkit.getPlayer(args[1]);
				if (target != null) {
					if (args[2].equalsIgnoreCase("emerald") || args[2].equalsIgnoreCase("lapis") || args[2].equalsIgnoreCase("gold") || args[2].equalsIgnoreCase("diamond")
							|| args[2].equalsIgnoreCase("redstone") || args[2].equalsIgnoreCase("iron") || args[2].equalsIgnoreCase("coal")) {
						sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/oreseeds give " + args[1] + " " + args[2] + " &c<#>"));
					} else {
						sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cPlease enter a valid ore seed."));
					}
				} else {
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cThat player is currently offline."));
				}
				break;
			case "giveall":
				if (args[1].equalsIgnoreCase("emerald") || args[1].equalsIgnoreCase("lapis") || args[1].equalsIgnoreCase("gold") || args[1].equalsIgnoreCase("diamond")
						|| args[1].equalsIgnoreCase("redstone") || args[1].equalsIgnoreCase("iron") || args[1].equalsIgnoreCase("coal")) {
					if (isInt(args[2])) {
						for (int i = 0; i < Integer.parseInt(args[2]); i++) {
							Bukkit.getOnlinePlayers().forEach(all -> all.getInventory().addItem(Core.getInstance().makeSeed(SeedType.valueOf(args[1].toUpperCase()))));
						}
					} else {
						sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cPlease enter a whole number."));
					}
				} else {
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cPlease enter a valid ore seed."));
				}
				break;
			}
			return true;
		}

		if (args.length == 4) {
			switch (args[0].toLowerCase()) {
			case "give":
				Player target = Bukkit.getPlayer(args[1]);
				if (target != null) {
					if (args[2].equalsIgnoreCase("emerald") || args[2].equalsIgnoreCase("lapis") || args[2].equalsIgnoreCase("gold") || args[2].equalsIgnoreCase("diamond")
							|| args[2].equalsIgnoreCase("redstone") || args[2].equalsIgnoreCase("iron") || args[2].equalsIgnoreCase("coal")) {
						if (isInt(args[3])) {
							for (int i = 0; i < Integer.parseInt(args[3]); i++) {
								target.getInventory().addItem(Core.getInstance().makeSeed(SeedType.valueOf(args[2].toUpperCase())));
							}
						} else {
							sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cPlease enter a whole number."));
						}
					} else {
						sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cPlease enter a valid ore seed."));
					}
				} else {
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cThat player is currently offline."));
				}
				break;
			}
			return true;
		}
		return true;
	}
}

package com.shadebyte.oreseeds;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import com.shadebyte.oreseeds.Core.SeedType;

public class SeedEvents implements Listener {

	@EventHandler
	public void onSeedPlace(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if (!p.hasPermission("oreseeds.use")) {
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cYou do not have permission to use ore seeds."));
			return;
		}

		if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {

			if (p.getItemInHand().isSimilar(Core.getInstance().makeSeed(SeedType.COAL))) {
				performTransition(SeedType.COAL, e.getClickedBlock());
			} else if (p.getItemInHand().isSimilar(Core.getInstance().makeSeed(SeedType.DIAMOND))) {
				performTransition(SeedType.DIAMOND, e.getClickedBlock());
			} else if (p.getItemInHand().isSimilar(Core.getInstance().makeSeed(SeedType.EMERALD))) {
				performTransition(SeedType.EMERALD, e.getClickedBlock());
			} else if (p.getItemInHand().isSimilar(Core.getInstance().makeSeed(SeedType.GOLD))) {
				performTransition(SeedType.GOLD, e.getClickedBlock());
			} else if (p.getItemInHand().isSimilar(Core.getInstance().makeSeed(SeedType.IRON))) {
				performTransition(SeedType.IRON, e.getClickedBlock());
			} else if (p.getItemInHand().isSimilar(Core.getInstance().makeSeed(SeedType.LAPIS))) {
				performTransition(SeedType.LAPIS, e.getClickedBlock());
			} else if (p.getItemInHand().isSimilar(Core.getInstance().makeSeed(SeedType.REDSTONE))) {
				performTransition(SeedType.REDSTONE, e.getClickedBlock());
			}

			if (p.getItemInHand().getAmount() >= 2) {
				p.getItemInHand().setAmount(p.getItemInHand().getAmount() - 1);
				p.updateInventory();
			} else {
				p.setItemInHand(null);
				p.updateInventory();
			}
		}
	}

	private void performTransition(SeedType type, Block block) {
		switch (type) {
		case COAL:
			block.getWorld().strikeLightningEffect(block.getLocation());
			block.setType(Material.COAL_ORE);
			break;
		case DIAMOND:
			block.getWorld().strikeLightningEffect(block.getLocation());
			block.setType(Material.DIAMOND_ORE);
			break;
		case EMERALD:
			block.getWorld().strikeLightningEffect(block.getLocation());
			block.setType(Material.EMERALD_ORE);
			break;
		case GOLD:
			block.getWorld().strikeLightningEffect(block.getLocation());
			block.setType(Material.GOLD_ORE);
			break;
		case IRON:
			block.getWorld().strikeLightningEffect(block.getLocation());
			block.setType(Material.IRON_ORE);
			break;
		case LAPIS:
			block.getWorld().strikeLightningEffect(block.getLocation());
			block.setType(Material.LAPIS_ORE);
			break;
		case REDSTONE:
			block.getWorld().strikeLightningEffect(block.getLocation());
			block.setType(Material.REDSTONE_ORE);
			break;
		default:
			break;
		}
	}
}

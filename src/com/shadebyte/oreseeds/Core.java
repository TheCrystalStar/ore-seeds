package com.shadebyte.oreseeds;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

public class Core extends JavaPlugin {

	private static Core instance;

	@Override
	public void onEnable() {
		instance = this;
		getConfig().options().copyDefaults(true);
		saveDefaultConfig();
		Bukkit.getPluginManager().registerEvents(new SeedEvents(), this);
		Bukkit.getPluginCommand("oreseeds").setExecutor(new OreSeedCMD());
	}
	
	@Override
	public void onDisable() {
		instance = null;
	}

	public static Core getInstance() {
		return instance;
	}

	protected enum SeedType {
		COAL("coal"), IRON("iron"), REDSTONE("redstone"), DIAMOND("diamond"), GOLD("gold"), LAPIS("lapis"), EMERALD("emerald");
		private String name;

		private SeedType(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}
	}

	public ItemStack makeSeed(SeedType type) {
		String item = getConfig().getString("seeds." + type.getName() + ".item");
		String[] arr = item.split(":");
		ItemStack is = new ItemStack(Material.valueOf(arr[0]), 1, Short.parseShort(arr[1]));
		ItemMeta ismeta = is.getItemMeta();
		ismeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', getConfig().getString("seeds." + type.getName() + ".name")));
		ArrayList<String> lore = new ArrayList<>();
		for (String list : getConfig().getStringList("seeds." + type.getName() + ".lore")) {
			lore.add(ChatColor.translateAlternateColorCodes('&', list));
		}
		ismeta.setLore(lore);
		is.setItemMeta(ismeta);
		return is;
	}

	public Inventory seedInventory() {
		Inventory inv = Bukkit.createInventory(null, 9, ChatColor.translateAlternateColorCodes('&', "&6&lOre &e&lSeeds"));
		for (SeedType type : SeedType.values()) {
			inv.addItem(makeSeed(type));
		}
		return inv;
	}
}
